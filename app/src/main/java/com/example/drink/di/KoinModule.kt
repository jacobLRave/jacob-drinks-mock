package com.example.drink.di

import com.example.drink.data.DrinkRepo
import com.example.drink.data.local.DrinkDatabase
import com.example.drink.data.remote.DrinkService
import com.example.drink.domain.GetCategoryUseCase
import com.example.drink.domain.GetDrinkDetailsUseCase
import com.example.drink.domain.GetDrinksUseCase
import com.example.drink.presentation.viewmodel.CategoryViewModel
import com.example.drink.presentation.viewmodel.DrinkDetailViewModel
import com.example.drink.presentation.viewmodel.DrinksViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.Koin
import org.koin.dsl.module

object KoinModule {

    val appModule = module {

        // Service
        single { DrinkService.drinkService() }
        // DAO
        single{DrinkDatabase.getInstance(get()).detailsDao()}
        single{DrinkDatabase.getInstance(get()).typesDao()}
        // Repo
        single { DrinkRepo() }
        // Use cases
        single { GetCategoryUseCase() }
        single { GetDrinksUseCase() }
        single { GetDrinkDetailsUseCase() }
        // View model
        viewModel {  CategoryViewModel() }
        viewModel { params ->  DrinksViewModel(params.get())}
        viewModel { params -> DrinkDetailViewModel(params.get())}

        factory { KoinModule }
    }

}



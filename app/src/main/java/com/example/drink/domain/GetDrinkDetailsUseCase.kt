package com.example.drink.domain

import com.example.drink.data.DrinkRepo
import com.example.drink.data.local.entity.Details
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent

class GetDrinkDetailsUseCase{
    private val  drinkRepo: DrinkRepo by KoinJavaComponent.inject(DrinkRepo::class.java)

    suspend operator fun invoke(id:Int):Result<Details> = withContext(Dispatchers.IO){
        return@withContext try{
            val response = drinkRepo.getDrinkById(id)
            val drinkList = response
            Result.success(drinkList)
        } catch (ex: Exception) {
            Result.failure(ex)
        }
    }
}
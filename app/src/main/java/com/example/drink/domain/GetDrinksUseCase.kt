package com.example.drink.domain

import com.example.drink.data.DrinkRepo
import com.example.drink.data.local.entity.Types
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent

class GetDrinksUseCase {
    private val drinkRepo: DrinkRepo by KoinJavaComponent.inject(DrinkRepo::class.java)

    suspend operator fun invoke(type: String):Result<List<Types>> = withContext(Dispatchers.IO) {


        return@withContext try {
            val response = drinkRepo.getDrinks(type)
            val drinksList = response
            Result.success(drinksList)
        } catch (ex: Exception) {
            Result.failure(ex)
        }
    }

    }

package com.example.drink.domain

import com.example.drink.data.DrinkRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent.inject

class GetCategoryUseCase{

    private val drinkRepo: DrinkRepo by inject(DrinkRepo::class.java)
    suspend operator fun invoke(): List<String> = withContext(Dispatchers.IO) {

        return@withContext drinkRepo.getCategories()

    }
}
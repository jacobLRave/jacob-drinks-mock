package com.example.drink.presentation.ui.drink

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.drink.databinding.FragmentDrinkDetailsBinding
import com.example.drink.presentation.viewmodel.DrinkDetailViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class DrinkDetailsFragment : Fragment() {

    private var _binding: FragmentDrinkDetailsBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DrinkDetailsFragmentArgs>()
    private val drinkDetailViewModel: DrinkDetailViewModel by viewModel {
        parametersOf(args.id)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinkDetailsBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drinkDetailViewModel.detailState.observe(viewLifecycleOwner) { state ->
            with(binding) {
                Glide.with(ivStrDrinkThumb).load(state.detail.strDrinkThumb).into(ivStrDrinkThumb)

                tvStrDrink.text = state.detail.strDrink
                tvStrCategory.text = state.detail.strCategory
                tvStrInstructions.text = state.detail.strInstructions
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}
package com.example.drink.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.drink.databinding.ItemCategoryBinding

class CategoryAdapter(val navigate: (String) -> Unit) :
    RecyclerView.Adapter<CategoryAdapter.InputViewHolder>() {

    private var category = mutableListOf<String>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InputViewHolder {
        val binding = ItemCategoryBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return InputViewHolder(binding)
    }

    override fun onBindViewHolder(holder: InputViewHolder, position: Int) {
        val category = category[position]
        holder.loadCategory(category)
        holder.navigateOnclick().setOnClickListener(){
            navigate(category)
        }
    }

    override fun getItemCount(): Int {
        return category.size
    }

    fun addCategory(category: List<String>) {
        this.category = category.toMutableList()
        notifyDataSetChanged()
    }

    class InputViewHolder(
        private val binding: ItemCategoryBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun loadCategory(category: String) {
            binding.tvCategory.text = category
        }

        fun navigateOnclick(): TextView {
            return binding.tvCategory
        }
    }
}
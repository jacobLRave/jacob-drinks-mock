package com.example.drink.presentation.ui.drink

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.drink.databinding.FragmentTypesBinding
import com.example.drink.presentation.ui.adapter.TypesAdapter
import com.example.drink.presentation.viewmodel.DrinksViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class TypesFragment : Fragment() {
    private var _binding: FragmentTypesBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<TypesFragmentArgs>()
    private val typesViewModel:DrinksViewModel by viewModel {
        parametersOf(args.drinkId)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentTypesBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        typesViewModel.drinksState.observe(viewLifecycleOwner){state ->
            binding.rvTypes.apply{
            adapter = TypesAdapter(::navigate).apply { addType(state.drinks) }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
fun navigate(id:Int){
    findNavController().navigate(TypesFragmentDirections.actionTypesFragmentToDrinkDetailsFragment(id))
}
}
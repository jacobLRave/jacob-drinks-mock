package com.example.drink.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.drink.data.local.entity.Types
import com.example.drink.databinding.ItemTypesBinding
import com.google.android.material.card.MaterialCardView

class TypesAdapter(val navigation: (Int) -> Unit) :
    RecyclerView.Adapter<TypesAdapter.InputViewHolder>() {
    private var types = mutableListOf<Types>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InputViewHolder {
        val binding = ItemTypesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return InputViewHolder(binding)
    }

    override fun onBindViewHolder(holder: InputViewHolder, position: Int) {
        var type = types[position]
        holder.loadTypes(type)
        holder.onClickNavigation().setOnClickListener() {
            navigation(type.idDrink)
        }
    }

    override fun getItemCount(): Int {
        return types.size
    }

    fun addType(type: List<Types>) {
        this.types = type.toMutableList()
    }

    class InputViewHolder(val binding: ItemTypesBinding) : RecyclerView.ViewHolder(binding.root) {
        fun loadTypes(type: Types) {
            Glide.with(binding.ivTypes).load(type.strDrinkThumb).into(binding.ivTypes)
            binding.tvType.text = type.strDrink
        }

        fun onClickNavigation(): MaterialCardView {
            return binding.mvTypes
        }

    }

}
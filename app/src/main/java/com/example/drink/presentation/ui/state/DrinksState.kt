package com.example.drink.presentation.ui.state

import com.example.drink.data.local.entity.Types

data class DrinksState (
    var isLoading:Boolean = false,
    var drinks:List<Types> = emptyList(),
    var errorMessage:String? = null,
    var type:String = ""
        )
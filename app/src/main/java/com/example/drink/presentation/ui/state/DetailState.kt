package com.example.drink.presentation.ui.state

import com.example.drink.data.local.entity.Details

data class DetailState(
    val detail: Details = Details(),
    val isLoading: Boolean = false,
    val errorMsg: String? = null
)


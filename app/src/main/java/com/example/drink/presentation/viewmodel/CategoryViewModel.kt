package com.example.drink.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.drink.domain.GetCategoryUseCase
import com.example.drink.presentation.ui.state.CategoryState
import org.koin.java.KoinJavaComponent.inject

class CategoryViewModel  : ViewModel() {
    private val  getCategoryUseCase: GetCategoryUseCase by inject(GetCategoryUseCase::class.java)

    val categoryState: LiveData<CategoryState> = liveData {
        emit(CategoryState(isLoading = true))
        val drinks = getCategoryUseCase()
        emit(CategoryState(category = drinks))
    }

}
package com.example.drink.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.drink.data.local.entity.Types
import com.example.drink.domain.GetDrinksUseCase
import com.example.drink.presentation.ui.state.DrinksState
import org.koin.java.KoinJavaComponent.inject

class DrinksViewModel(drinkId: String) : ViewModel() {
  private val getDrinksUseCase:GetDrinksUseCase by inject(GetDrinksUseCase::class.java)

    val drinksState: LiveData<DrinksState> = liveData {
        emit(DrinksState(isLoading = true))
        val drinksResult: Result<List<Types>> = getDrinksUseCase(drinkId)
        val state = DrinksState(
            drinks = drinksResult.getOrNull()?: emptyList(),
            errorMessage = drinksResult.exceptionOrNull()?.message
        )
        emit(state)

    }
}


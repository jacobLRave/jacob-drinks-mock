package com.example.drink.presentation.ui.drink

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.drink.R
import com.example.drink.databinding.FragmentDrinkBinding
import com.example.drink.presentation.ui.adapter.CategoryAdapter
import com.example.drink.presentation.viewmodel.CategoryViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class DrinkFragment : Fragment(R.layout.fragment_drink) {

    private var _binding: FragmentDrinkBinding? = null
    private val binding get() = _binding!!
    private val categoryViewModel:CategoryViewModel by viewModel()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinkBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        categoryViewModel.categoryState.observe(viewLifecycleOwner) { state ->
            binding.rvCategory.apply {
                adapter = CategoryAdapter(::navigate).apply { addCategory(state.category) }
            }
        }

    }
    fun navigate(type:String){
        findNavController().navigate(DrinkFragmentDirections.actionDrinkFragmentToTypesFragment(type))
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
package com.example.drink.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.drink.data.local.entity.Details
import com.example.drink.domain.GetDrinkDetailsUseCase
import com.example.drink.presentation.ui.state.DetailState
import org.koin.java.KoinJavaComponent.inject

class DrinkDetailViewModel(val itemId: Int) : ViewModel() {

    private val getDrinkDetailsUseCase: GetDrinkDetailsUseCase by inject(GetDrinkDetailsUseCase::class.java)

    val detailState: LiveData<DetailState> = liveData {
        emit(DetailState(isLoading = true))
        val detailsResult: Result<Details> = getDrinkDetailsUseCase(itemId)
        val state = DetailState(
            detail = detailsResult.getOrNull() ?: Details(),
            errorMsg = detailsResult.exceptionOrNull()?.message
        )
        emit(state)
    }

}
package com.example.drink.presentation.ui.state

import com.example.drink.data.remote.response.CategoryDTO

data class CategoryState(
    val category: List<String> = emptyList(),
    val isLoading: Boolean = false,
    val errorMsg: String? = null
)

package com.example.drink.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Details(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val idDrink: String = "",
    val strCategory: String = "",
    val strDrink: String = "",
    val strDrinkThumb: String = "",
    val strInstructions: String = "",
)

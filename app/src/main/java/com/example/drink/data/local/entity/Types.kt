package com.example.drink.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Types(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val idDrink: Int =0,
    val strDrink: String ="",
    val strDrinkThumb: String =""
)

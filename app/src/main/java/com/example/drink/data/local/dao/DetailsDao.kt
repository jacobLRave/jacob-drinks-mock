package com.example.drink.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.drink.data.local.entity.Details

@Dao
interface DetailsDao {
    @Query("SELECT * FROM Details")
    suspend fun getAll(): List<Details>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDetail(detail:Details)
}

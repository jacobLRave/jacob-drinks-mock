package com.example.drink.data

import com.example.drink.data.local.dao.DetailsDao
import com.example.drink.data.local.dao.TypesDao
import com.example.drink.data.local.entity.Details
import com.example.drink.data.local.entity.Types
import com.example.drink.data.remote.DrinkService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent.inject

class DrinkRepo {
    private val drinkService: DrinkService by inject(DrinkService::class.java)
    private val detailsDao: DetailsDao by inject(DetailsDao::class.java)
    private val typesDao: TypesDao by inject(TypesDao::class.java)
    suspend fun getCategories() =
        withContext(Dispatchers.IO) {
            drinkService.getCatagories().categoryItems.map { it.strCategory }
        }

    suspend fun getDrinks(type: String) =
        withContext(Dispatchers.IO) {
            return@withContext typesDao.getTypes().ifEmpty {
                val types = drinkService.getDrinks(type).drinks.map {
                    Types(
                        idDrink = it.idDrink.toInt(),
                        strDrink = it.strDrink,
                        strDrinkThumb = it.strDrinkThumb
                    )
                }
                typesDao.insertType(*types.toTypedArray())
                return@withContext types
            }
        }

    suspend fun getDrinkById(id: Int):Details = withContext(Dispatchers.IO) {

       val detailsCache=  detailsDao.getAll().ifEmpty {
            val details = drinkService.getDrinkById(id).drinks.map {
                Details(
                    idDrink = it.idDrink,
                    strCategory = it.strCategory,
                    strDrink = it.strDrink,
                    strDrinkThumb = it.strDrinkThumb,
                    strInstructions = it.strInstructions
                )
            }
            detailsDao.insertDetail(details[0])
            return@ifEmpty details
        }
        return@withContext detailsCache[0]
    }
}
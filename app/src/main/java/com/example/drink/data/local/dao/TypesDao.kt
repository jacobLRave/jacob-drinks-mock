package com.example.drink.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.drink.data.local.entity.Types

@Dao
interface TypesDao {
    @Query("SELECT * FROM Types")
    suspend fun getTypes(): List<Types>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertType(vararg type: Types)
}
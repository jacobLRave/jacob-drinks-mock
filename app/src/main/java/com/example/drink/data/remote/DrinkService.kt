package com.example.drink.data.remote

import com.example.drink.data.remote.response.CategoryDTO
import com.example.drink.data.remote.response.DrinkDetailsDTO
import com.example.drink.data.remote.response.DrinksDTO
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface DrinkService {

    companion object {
        const val BASE_URL = "https://www.thecocktaildb.com"
        const val QUERY_CATAGORY = "c"
        fun drinkService(): DrinkService {
            val retrofit: Retrofit =
                Retrofit.Builder()
                    .baseUrl(DrinkService.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit.create(DrinkService::class.java)
        }
    }

    @GET("/api/json/v1/1/list.php")
    suspend fun getCatagories(@Query(QUERY_CATAGORY) type: String = "list"): CategoryDTO
    @GET("/api/json/v1/1/filter.php")
    suspend fun  getDrinks(@Query(QUERY_CATAGORY)type:String): DrinksDTO
    @GET("/api/json/v1/1/lookup.php")
    suspend fun getDrinkById(@Query("i")type:Int):DrinkDetailsDTO
}

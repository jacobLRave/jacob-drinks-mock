package com.example.drink.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.drink.data.local.dao.DetailsDao
import com.example.drink.data.local.dao.TypesDao
import com.example.drink.data.local.entity.Details
import com.example.drink.data.local.entity.Types

@Database(entities = [Details::class, Types::class], version = 1, exportSchema = false)
abstract class DrinkDatabase : RoomDatabase() {

    abstract fun detailsDao():DetailsDao
    abstract fun typesDao():TypesDao

    companion object {
        private const val DB_NAME = "notes.db"

        @Volatile
        private var instance: DrinkDatabase? = null

        private fun databaseBuilder(context: Context): DrinkDatabase = Room.databaseBuilder(
            context, DrinkDatabase::class.java,
            DB_NAME
        ).build()

        fun getInstance(context: Context): DrinkDatabase {
            return instance ?: synchronized((this)) {
                instance ?: databaseBuilder(context).also { instance = it }
            }
        }
    }
}